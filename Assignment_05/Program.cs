﻿using System;
using System.Collections.Generic; // used when testing via list

namespace Assignment_05
{
    class Program
    {
        static void Main(string[] args)
        {
            // This program generates joint normally distributed random values
            // given an input correlation value and chosen generation algorithm

            Random RandomInstance = new Random(); // initiate variable that generates a uniformally distributed random value between 0 and 1
            bool SecondAvailable = false; // initiate boolean that is used to check for an already calculated second normally distributed random value
            double SecondValue = 0; // initiate second normally distributed random value.
            double Rho; // initate rho variable
            int Algo; // initiate generation variable

            /* //USED TO TEST OUTPUT
            List<long> list = new List<long>(24) { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            for (long i = 0; i < 10000000; i++)
            {
                double Value = GetJointNormalPolarRejection(RandomInstance, SecondAvailable, SecondValue,-1);

                int Index = Convert.ToInt16((Value + 6) * 2);
                list[Index]++;

            }
            */

            do // error handle input outside of range
            {
                Console.Write("Input correlation value (between -1 and 1): ");
                string RhoString = Console.ReadLine();
                Rho = Convert.ToDouble(RhoString); // convert input to double
            } while (Math.Abs(Rho) > 1);

            do // error handle input outside of range
            {
                Console.Write("Input generation alogrithm (1 = 12 value; 2 = Box-Muller; 3 = Polar Rejection): ");
                string AlgoString = Console.ReadLine();
                Algo = Convert.ToInt16(AlgoString); // convert input to int
            } while (Algo != 1 && Algo != 2 && Algo != 3);

            if (Rho == 0.0) // choose generation algo per user input. if user inputs 0, computing power can be saved as Z1 = Z2 for joint normally distributed random variables
            {
                if (Algo == 1)
                {
                    Console.WriteLine(Get12ValueRandom(RandomInstance));
                }
                if (Algo == 2)
                {
                    Console.WriteLine(GetBoxMuller(RandomInstance, SecondAvailable, SecondValue));
                }
                if (Algo == 3)
                {
                    Console.WriteLine(GetPolarRejection(RandomInstance, SecondAvailable, SecondValue));
                }
            }
            else // choose generation algo per user input.
            {
                if (Algo == 1)
                {
                    Console.WriteLine(GetJointNormal12ValueRandom(RandomInstance, SecondAvailable, SecondValue, Rho));
                }
                if (Algo == 2)
                {
                    Console.WriteLine(GetJointNormalBoxMuller(RandomInstance, SecondAvailable, SecondValue, Rho));
                }
                if (Algo == 3)
                {
                    Console.WriteLine(GetJointNormalPolarRejection(RandomInstance, SecondAvailable, SecondValue, Rho));
                }
            }

            Console.ReadLine(); // leave window open

        }

        static double Get12ValueRandom(Random RandomInstance) // used when Rho = 0 
        {

            double randomNumber = 0;
            for (int i = 0; i < 12; i++) // sum twelve uniform random numbers  
            {
                randomNumber = randomNumber + RandomInstance.NextDouble();
            }
            return (randomNumber - 6); // returns normal random value
        }

        static double GetBoxMuller(Random RandomInstance, bool SecondAvailable, double SecondValue) // used when Rho = 0 
        {
            if (SecondAvailable == false) // test for second normally distributed random value
            {
                double X1 = RandomInstance.NextDouble();
                double X2 = RandomInstance.NextDouble();

                double Z1 = Math.Sqrt(-2 * Math.Log(X1)) * Math.Cos(2 * Math.PI * X2);
                SecondValue = Math.Sqrt(-2 * Math.Log(X1)) * Math.Sin(2 * Math.PI * X2); // Z2
                SecondAvailable = true; // set second available condition to true as two are calculated
                return (Z1);
            }
            SecondAvailable = false; // when function is called 2nd time, second available condition set to false so that 2 more joint normally distributed values will be calculated when method is run again thus resetting the process
            return (SecondValue); // Z2 returned
        }

        static double GetPolarRejection(Random RandomInstance, bool SecondAvailable, double SecondValue) // used when Rho = 0 
        {

            double X1; // initiate variables
            double X2;
            double W;

            if (SecondAvailable == false) // test for second normally distributed random value
            {
                do
                {
                    X1 = (RandomInstance.NextDouble() - 0.5) * 2; // generates uniform random value between -1 and 1
                    X2 = (RandomInstance.NextDouble() - 0.5) * 2; // generates uniform random value between -1 and 1
                    W = (X1 * X1) + (X2 * X2);
                } while (W > 1); // loops until W < 1

                double C = Math.Sqrt(-2 * Math.Log(W) / W);

                double Z1 = C * X1; // Z1
                SecondValue = C * X2; // Z2
                SecondAvailable = true; // set second available condition to true as two are calculated
                return (Z1); // Z1 returned
            }
            SecondAvailable = false;// when function is called 2nd time, second available condition set to false so that 2 more joint normally distributed values will be calculated when method is run again thus resetting the process
            return (SecondValue); // Z2 returned
        }

        static double GetJointNormal12ValueRandom(Random RandomInstance, bool SecondAvailable, double SecondValue, double Rho) // two uniform random numbers used to calculate Z1 and Z2. used when Rho != 0
        {

            if (SecondAvailable == false) // test for second normally distributed random value
            {
                double randomNumber1 = 0;
                double randomNumber2 = 0;
                for (int i = 0; i < 12; i++) // sum twelve uniform random numbers for joint normally distributed values
                {
                    randomNumber1 = randomNumber1 + RandomInstance.NextDouble();
                    randomNumber2 = randomNumber2 + RandomInstance.NextDouble();
                }
                double E1 = randomNumber1 - 6; // first normal random value
                double E2 = randomNumber1 - 6; // second normal random value
                SecondValue = Rho * E1 + Math.Sqrt(1 - (Rho * Rho)) * E2;// Z2 = SecondValue
                SecondAvailable = true; // set second available condition to true as two are calculated
                return (E1); // Z1 retuned. Z1 = E1

            }
            SecondAvailable = false; // when function is called 2nd time, second available condition set to false so that 2 more joint normally distributed values will be calculated when method is run again thus resetting the process
            return (SecondValue); // Z2
        }

        static double GetJointNormalBoxMuller(Random RandomInstance, bool SecondAvailable, double SecondValue, double Rho) // used when Rho != 0
        {
            if (SecondAvailable == false) // test for second normally distributed random value
            {
                double X1 = RandomInstance.NextDouble();
                double X2 = RandomInstance.NextDouble();

                double E1 = Math.Sqrt(-2 * Math.Log(X1)) * Math.Cos(2 * Math.PI * X2); // first normal random value
                double E2 = Math.Sqrt(-2 * Math.Log(X1)) * Math.Sin(2 * Math.PI * X2); // second normal random value
                SecondValue = Rho * E1 + Math.Sqrt(1 - (Rho * Rho)) * E2;// Z2 = SecondValue
                SecondAvailable = true; // set second available condition to true as two are calculated
                return (E1); // Z1 returned. Z1 = E1
            }
            SecondAvailable = false; // when function is called 2nd time, second available condition set to false so that 2 more joint normally distributed values will be calculated when method is run again thus resetting the process
            return (SecondValue); // Z2 retuned
        }

        static double GetJointNormalPolarRejection(Random RandomInstance, bool SecondAvailable, double SecondValue, double Rho) // used when Rho != 0
        {
            double X1; // initiate variables
            double X2;
            double W;

            if (SecondAvailable == false) // test for second normally distributed random value
            {
                do
                {
                    X1 = (RandomInstance.NextDouble() - 0.5) * 2; // generates uniform random value between -1 and 1
                    X2 = (RandomInstance.NextDouble() - 0.5) * 2; // generates uniform random value between -1 and 1
                    W = (X1 * X1) + (X2 * X2);
                } while (W > 1); // loops until W < 1

                double C = Math.Sqrt(-2 * Math.Log(W) / W);

                double E1 = C * X1; // first normal random value
                double E2 = C * X2; // second normal random value

                SecondValue = Rho * E1 + Math.Sqrt(1 - (Rho * Rho)) * E2; // Z2 = SecondValue
                SecondAvailable = true; // set second available condition to true as two are calculated
                return (E1); // Z1 returned. Z1 = E1
            }
            SecondAvailable = false; // when function is called 2nd time, second available condition set to false so that 2 more joint normally distributed values will be calculated when method is run again thus resetting the process
            return (SecondValue); // Z2 returned
        }
    }
}
